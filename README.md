# Fortinet Project Assignment :: Backend Developer Task

# Overview: Hash Scanner using VirusTotal API

The purpose of this project is to create a web application that allows users to upload a text file of MD5 or Sha256 hashes and generates a report using VirusTotal's public API. The report displays relevant information such as Fortinet detection name, number of engines detected, and scan date for each hash.

The project is deployed on AWS. The link is:
[Fortinet Software Developer Coding Project](http://ec2-18-236-182-182.us-west-2.compute.amazonaws.com/)


## Project Execution
There are 2 different ways to execute the command.

**The first method is to clone the repositry and run docker-compose command.** 
1. Clone the repositry by running the following command.
```
git clone https://gitlab.com/saurabhsingh0/fortinet-project-assignment-for-software-backend-developer.git
``` 
2. CD to that directory
```
cd fortinet-project-assignment-for-software-backend-developer
```
3. Run the docker-compose command for starting the project and redis cahche containers.
```
docker-compose up --build
```  
<br>

**Another way is to pull the docker image of the project and redis cache by executing the following commands on the terminal:**

1. Pull the project docker image.
```
docker pull saurabhsingh0/fortinet_task
```
2. Pull the redis docker image.
```
docker pull redis
```
3. Create a network for project and redis cache for communication.
```
docker network create redis-flask
```
4. Start the redis server container.
```
docker run --name redis --network redis-flask -d redis
```
5. Start the project container.
```
docker run --network redis-flask -p 5000:5000 saurabhsingh0/fortinet_task
```

## Problems Tackled

To improve query times and reduce the load on the VirusTotal API, a caching system has been implemented using Redis. When a file hash is queried, the application first checks if it is in the Redis cache. If the hash is in the cache, the application retrieves the results from the cache and displays them to the user. If the hash is not in the cache, the application makes a GET request to the VirusTotal API and stores the results in the cache for future use.

The caching system stores the results of the VirusTotal API for a particular file hash for a period of up to 1 day. This ensures that if a user queries the same hash multiple times within a 24-hour period, the website will retrieve the results from the cache rather than making a new API request to VirusTotal. To enable easy access to previous results, a unique URL is generated for each file that a user submits. Users can revisit the URL to view the results of the scan, and the website will retrieve the data from the cache, avoiding the need for a new API request.

## Executive Summary

```
├── Dockerfile
├── README.MD
├── app.py
├── docker-compose.yml
├── requirements.txt
└── templates
    ├── base.html
    ├── content.html
    ├── error.html
    └── index.html
```

- ```Dockerfile```: used to build an image for the application.

- ```docker-compose.yml```: contains the configuration for multiple containers, such as the Redis container and the application container, which are used together to run the application.

- ```app.py```: contains the main application code written in Python using the Flask framework. This file contains the code for the API requests to VirusTotal and for the caching mechanism implemented using Redis. The templates directory contains the HTML templates for rendering the user interface of the application.

## Technology Stack

- Python3
- Redis
- Flask
- HTML, CSS & JavaScript
- Docker

## Required Dependencies

- Docker should be installed on the system.

## Screenshots

1. Homepage

![HomePage](images/Homepage.png)

2. Result 

![Result](images/Result1.png)

3. HomePage with URLs

![HomePageURL](images/homepage2.png)

4. Redis Cache

![Cache](images/rediscache.png)

5. Error Screen

![Error](images/error.png)

## Assumptions

1. It has been assumed that any user can submit the input file as there is currently no authentication method in place to restrict access to the submission feature.
2. The text file that is submitted by the user is always and there is no error handling done for incorrect or invalid submissions.
