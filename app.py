import aiohttp, asyncio
import redis, json, secrets

from flask import Flask, render_template, request, redirect, url_for, session
from datetime import datetime, timedelta

app = Flask(__name__)
app.secret_key = 'secret_key'

headers = {
    "accept": "application/json",
    "x-apikey": "6a88bebef21e4684af59ebed74484773b1ffd622c9efb337f6cd5c3840d19edd"
}

redis_client = redis.Redis(host="redis", port=6379, db=0)



@app.route('/', methods=['GET', 'POST'])
def index():
    """
    The route for the home page
    It renders index.html where the user can submit a text file which contains to upload a text
    file as a list of hashes (MD5 or Sha256)
    Once the user clicks on submit button it redirects it to the result
    """
    session.clear()
    if request.method == 'POST':
        user_uploaded_file = request.files['input_file']        #read the file uploaded by user and parse it's content
        contents = user_uploaded_file.read().decode()
        file_hash_list = contents.split("\n")

        #generate a text(query_id) for every file uploaded by the user that is not present in the cache
        query_id = generate_random_string()
        while redis_client.get(query_id) is not None:
            query_id = generate_random_string()
        
        redis_client.set(query_id, json.dumps(file_hash_list), ex=86400)
        #session['redirected'] = True
        print(query_id)
        return redirect(url_for("results", query_id=query_id))
    
    #get all the recently processed files
    files_list=[]
    for files in redis_client.keys():
        files = files.rstrip()
        if len(files) == 6:
            files_list.append(files.decode())
    #print(files_list)
    return render_template('index.html', length = len(files_list), files=files_list)

@app.route('/results/<string:query_id>', methods = ['GET'])
def results(query_id):
    """
    The route for the results for a givem query_id
    It renders index.html where the user can submit a text file which contains to upload a text
    file as a list of hashes (MD5 or Sha256)
    Once the user clicks on submit button it redirects it to the result
    """
    print(query_id, "redirected")
    #check if the given file_id is in redis cache
    if redis_client.get(query_id) is None:
        return render_template('error.html') 

    #check if the redirected key is set:
    print(session.keys(), 'redirected' not in session.keys())
    if 'redirected' not in session.keys() :
        print('redirected')
        session['redirected'] = True
        app.permanent_session_lifetime = timedelta(seconds=5)
        return render_template('content.html', length = len([]), data = []) 

    session.clear()
    file_hash_list = json.loads(redis_client.get(query_id))
    file_resuts = asyncio.run(process_files(file_hash_list))
           
    return render_template('content.html', length = len(file_resuts), data = file_resuts) 



def generate_random_string(length = 6):
    """
    generate a random string of length 6 for that particular file
    """
    alphabet = "abcdefghijklmnopqrstuvwxyz0123456789"
    return ''.join(secrets.choice(alphabet) for i in range(length))


#get the file result for the all the file_hash as
async def process_files(file_hash_list):
    """
    The function takes list of file_hash as input and returns a result for every file 
    by making a asynchronus get request to VirusTotal API
    """
    async with aiohttp.ClientSession() as session:
        tasks = []
        for file_hash in file_hash_list:
            #check if the file_hash is in redis cache
            #sleep(1.5)
            tasks.append(asyncio.ensure_future(get_file_report_VirusTotal(session, file_hash.rstrip())))
        
        file_reports = await asyncio.gather(*tasks)
        return file_reports 


async def get_file_report_VirusTotal(session, file_hash):
    #check if the file_hash is in redis cache
    file_result = redis_client.get(file_hash)
    if file_result is None:
        file_result = {}
        url = f"https://www.virustotal.com/api/v3/files/{file_hash}"
        async with session.get(url, headers=headers) as resp:
            vt_resp = await resp.json()
            
            file_result['hash_value'] = file_hash
            file_result['scan_date'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            if resp.status == 200:
                analysis_result = vt_resp['data']["attributes"]["last_analysis_results"]
                #check if fortinet is present in analysis result
                if "Fortinet" in analysis_result.keys():
                    file_result['fortinet_detection_name'] = analysis_result['Fortinet']['result']
                else:
                    file_result['fortinet_detection_name'] = "Not Detected!"
                
                #count the vendors that have flagged the file as malicious 
                count = 0
                for _, vendor_result  in analysis_result.items():
                    if vendor_result['category'] == "malicious":
                        count += 1

                file_result['no_of_engines_detected'] = count
                
            else:
                file_result['fortinet_detection_name'] = vt_resp['error']['message']
                file_result['no_of_engines_detected'] = 0

        redis_client.set(file_hash, json.dumps(file_result), ex = 86400)
    
    else:
        file_result = json.loads(file_result)
    return file_result